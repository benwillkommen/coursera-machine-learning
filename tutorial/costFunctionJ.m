function J = costFunctionJ(X, y, theta)

% X is the "design matrix" containing our training examples.
% y is the class labels (outputs?)

m = size(X, 1);						% number of training examples
predictions = X*theta;				% predections of hypothesis on all m examples
sqrErrors = (predictions-y) .^2; 	% squared errors (element-wise exponent operator)

J = 1/(2*m) * sum (sqrErrors);