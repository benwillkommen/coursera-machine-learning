function [C, sigma] = dataset3Params(X, y, Xval, yval)
%EX6PARAMS returns your choice of C and sigma for Part 3 of the exercise
%where you select the optimal (C, sigma) learning parameters to use for SVM
%with RBF kernel
%   [C, sigma] = EX6PARAMS(X, y, Xval, yval) returns your choice of C and 
%   sigma. You should complete this function to return the optimal C and 
%   sigma based on a cross-validation set.
%

% You need to return the following variables correctly.
C = 1;
sigma = 0.3;

% ====================== YOUR CODE HERE ======================
% Instructions: Fill in this function to return the optimal C and sigma
%               learning parameters found using the cross validation set.
%               You can use svmPredict to predict the labels on the cross
%               validation set. For example, 
%                   predictions = svmPredict(model, Xval);
%               will return the predictions on the cross validation set.
%
%  Note: You can compute the prediction error using 
%        mean(double(predictions ~= yval))
%


paramValues = [0.01 0.03 0.1 0.3 1 3 10 30];

bestError = 1;

models = [];
try
	load('models.mat');
catch
end

if (rows(models) == 0)
	for i = 1:length(paramValues)
		for j = 1:length(paramValues)

			Ctest = paramValues(i);
			sigmatest = paramValues(j);


			%fprintf('\nCtest: %f\nsigmatest: %f\n', Ctest, sigmatest);

			model = svmTrain(X, y, Ctest, @(x1, x2) gaussianKernel(x1, x2, sigmatest));
			model.C = Ctest;
			model.sigma = sigmatest;

			models = [models; model];			

		end
	end
	try
		save('models.mat', 'models');
	catch
	end
end

for k = 1:rows(models)
	model = models(k);

	predictions =  svmPredict(model, Xval);

	modelError = mean(double(predictions ~= yval));
	%fprintf('\nmodelError: %f\n', modelError);

	if(modelError < bestError)
		%fprintf('\nBetter params chosen!\n');
		bestError = modelError;		
		C = model.C;
		sigma = model.sigma;
	end
end



% =========================================================================

end
